// const path = require("path");
// const http = require("http");
// const express = require("express");
// const port = process.env.PORT || 3000;
const Server = require('signal-fire').Server

// const publicPath = path.join(__dirname, "./public");

// const app = express();
// const appServer = http.createServer(app);
// app.use(express.static(publicPath));

const WebSocketServer = require('ws').Server

const server = new Server({
  engine: WebSocketServer,
  port: 7788
})

server.on('add_peer', peer => {
  console.log(`Added peer with peerId ${peer.peerId}`)
})

server.on('remove_peer', peerId => {
  console.log(`Removed peer with peerId ${peerId}`)
})

server.start().then(() => {
  console.log('Server started')
})


// appServer.listen(port, () => {
//   console.log(`Server port: ${port}`);
// });
